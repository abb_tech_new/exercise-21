package org.abbtech.exercise.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    //WebSecurityConfigurerAdapter bu clasi tapmadigi ucun error verir

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}admin123") // Bu sadece örnektir, gerçek bir uygulamada parolaları güvenli bir şekilde saklamalısınız.
                .roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/api/user/register", "/api/user/login").permitAll() // Herkesin erişebileceği end pointler
                .antMatchers("/api/user/**", "/api/post/**", "/api/comment/**").hasRole("ADMIN") // Sadece ADMIN rolüne sahip kullanıcıların erişebileceği end pointler
                .anyRequest().authenticated() // Diğer tüm isteklerin kimlik doğrulama gerektireceği
                .and()
                .httpBasic();
    }
}